import numpy as np
from pandas import read_csv
from sklearn.ensemble import RandomForestRegressor
from sklearn.cross_validation import KFold
from sklearn.cross_validation import cross_val_score

data = read_csv('abalone.csv')
data['Sex'] = data['Sex'].map(lambda x: 1 if x == 'M' else -1 if x == 'F' else 0)

# data = data[:5]

target = data['Rings']
features = np.delete(data.as_matrix(), 8, 1)

n = len(target)
n_folds = 5
maxTreeCount = 50
folds = KFold(n, n_folds=n_folds, shuffle=True, random_state=1)

minTreeNeeded = maxTreeCount + 1
neededScore = 0.52

for i in xrange(1, maxTreeCount + 1):
    forest = RandomForestRegressor(n_estimators=i, random_state=1)
    scores = cross_val_score(forest, features, target, scoring='r2', cv=folds)
    scores_sum = sum(scores)
    loc_min_acc = scores_sum / n_folds
    print scores, loc_min_acc
    if (loc_min_acc >= neededScore and minTreeNeeded > i):
        minTreeNeeded = i

print minTreeNeeded