from sklearn.decomposition import PCA
from pandas import read_csv
import numpy as np

trainData = read_csv('close_prices.csv')
companies = trainData.keys()
trainData = np.delete(trainData.as_matrix(), 0, 1)

pca = PCA(n_components=10)
pca.fit(trainData)

i = 0
p = 0
while (p < 0.9):
    p += pca.explained_variance_ratio_[i]
    i += 1

# min components count for dispersion percent 90
print p, i

trainTransformed = pca.transform(trainData)
firstComponentTransformed = trainTransformed.T[0]

dj_index = read_csv('djia_index.csv')
dj_index = np.delete(dj_index.as_matrix(), 0, 1)
dj_index = dj_index.T[0]

firstComponentTransformed = map(lambda x: np.float64(x), firstComponentTransformed)
dj_index = map(lambda x: np.float64(x), dj_index)

# calc correlation between first component companies and Dow Jones index
print np.corrcoef(firstComponentTransformed, dj_index)

# calc maxWeight for first component    
maxWeightIndex = -1
maxWeight = 0
for i in xrange(0, len(pca.components_[0])):
    weight = (pca.components_[0][i])
    if maxWeight < weight:
        maxWeight = weight
        maxWeightIndex = i

maxWeightIndex += 1

print pca.components_[0]
print maxWeight, maxWeightIndex, companies[maxWeightIndex]