import pandas
import numpy as np
from sklearn.tree import DecisionTreeClassifier

passengers = pandas.read_csv('../titanic-task/passengers.csv')
passengers = passengers[np.isfinite(passengers['Age'])]

classes = passengers['Pclass'].values
fares = passengers['Fare'].values
ages = passengers['Age'].values
sexes = [ 1 if v == 'male' else 0 for v in passengers['Sex'].values ]

features = []

for i in xrange(0, len(classes)):
    features.append([ classes[i], fares[i], ages[i], sexes[i] ])

samples = passengers['Survived'].values

clf = DecisionTreeClassifier(random_state=241)
clf.fit(features, samples)

print clf.feature_importances_
print clf.predict([[ 2, 25, 24, 1 ], [ 2, 25, 26, 0 ]])