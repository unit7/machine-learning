import numpy as np
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from pandas import read_csv

trainData = read_csv('perceptron-train.csv', header=None)
trainClasses = trainData.iloc[:,0].values
trainProps = np.delete(trainData.as_matrix(), 0, 1)

testData = read_csv('perceptron-test.csv', header=None)
testClasses = testData.iloc[:,0].values
testProps = np.delete(testData.as_matrix(), 0, 1)

perceptron = Perceptron(random_state=241)
perceptron.fit(trainProps, trainClasses)

testClassesPredict = perceptron.predict(testProps)

firstAccuracy = accuracy_score(testClasses, testClassesPredict)

print firstAccuracy

sc = StandardScaler()
sc.fit(trainProps)
trainPropsTransformed = sc.transform(trainProps)
testPropsTransformed = sc.transform(testProps)

perceptron.fit(trainPropsTransformed, trainClasses)
testClassesPredictNorm = perceptron.predict(testPropsTransformed)

secondAccuracy = accuracy_score(testClasses, testClassesPredictNorm)

print secondAccuracy
print (secondAccuracy - firstAccuracy)