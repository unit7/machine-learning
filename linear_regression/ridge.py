import numpy as np
from pandas import read_csv
from sklearn.feature_extraction.text import  TfidfVectorizer
from sklearn.feature_extraction import DictVectorizer
from scipy.sparse import hstack
from scipy.sparse import coo_matrix
from sklearn.linear_model import Ridge


train = read_csv('salary-train.csv')
test = read_csv('salary-test.csv')

# text -> lower, keep only letters and numbers
train['FullDescription'] = map(lambda t: t.lower(),
                               train['FullDescription'].replace('[^a-zA-Z0-9]', ' ', regex=True))
test['FullDescription'] = map(lambda t: t.lower(),
                              test['FullDescription'].replace('[^a-zA-Z0-9]', ' ', regex=True))

# calc TF-IDF - convert words to features. Each vaccancy has n features extracted from description
vectorizer = TfidfVectorizer(min_df=5)
tfidf = vectorizer.fit_transform(train['FullDescription'])
tfidfTest = vectorizer.transform(test['FullDescription']).toarray()

# fill skips
train['LocationNormalized'].fillna('nan', inplace=True)
train['ContractTime'].fillna('nan', inplace=True)
test['LocationNormalized'].fillna('nan', inplace=True)
test['ContractTime'].fillna('nan', inplace=True)

# convert string features to binary
dictVectorizer = DictVectorizer()
trainLocAndTimeFeatures = dictVectorizer \
    .fit_transform(train[['LocationNormalized', 'ContractTime']].to_dict('records')).toarray()
testLocAndTimeFeatures = dictVectorizer \
    .transform(test[['LocationNormalized', 'ContractTime']].to_dict('records')).toarray()

# union all calculated features
tfidfMatrix = coo_matrix(tfidf)
trainLocAndTimeMatrix = coo_matrix(trainLocAndTimeFeatures)
trainTargets = train['SalaryNormalized'].as_matrix()
trainFeatures = hstack([tfidfMatrix, trainLocAndTimeMatrix])

tfidfTestMatrix = coo_matrix(tfidfTest)
testLocAndTimeMatrix = coo_matrix(testLocAndTimeFeatures)
testFeatures = hstack([tfidfTestMatrix, testLocAndTimeMatrix]).toarray()


# fit model
ridge = Ridge(alpha=1, random_state=241)
ridge.fit(trainFeatures, trainTargets)

predicted = ridge.predict(testFeatures)

for i in xrange(0, len(predicted)):
    print predicted[i]

