from sklearn.svm import SVC
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import KFold
from sklearn import datasets
from sklearn.feature_extraction.text import  TfidfVectorizer
import numpy as np


def cmp_coef(x, y):
    c1 = abs(x[0])
    c2 = abs(y[0])
    return -1 if c1 > c2 else 1 if c1 < c2 else 0

def importantFeatures(coef, names):
    pairs = zip(coef, names)
    return sorted(pairs, cmp=cmp_coef)[:10]

newsgroups = datasets.fetch_20newsgroups(subset='all', categories=[ 'alt.atheism', 'sci.space' ])

tfidfVectorizer =TfidfVectorizer()
tfidf = tfidfVectorizer.fit_transform(newsgroups.data, newsgroups.target).toarray()

learnFeatures = tfidf[:50]
learnTargets = newsgroups.target[:50]

n = len(learnFeatures)
print n

feature_mapping = tfidfVectorizer.get_feature_names()

grid = {'C': np.power(10.0, np.arange(-5, 6))}
cv = KFold(n=n, n_folds=5, shuffle=True, random_state=241)
svc = SVC(kernel='linear', random_state=241)
gridSearch = GridSearchCV(svc, grid, scoring='accuracy', cv=cv)

gridSearch.fit(learnFeatures, learnTargets)

bestEstimator = gridSearch.best_estimator_
print 'best:' + str(bestEstimator)

bestEstimator.fit(tfidf, newsgroups.target)

coef = bestEstimator.coef_[0]

print importantFeatures(coef, feature_mapping)