from sklearn.svm import SVC
from pandas import read_csv
import numpy as np

svmData = read_csv('svm-data.csv', header=None)
svmAnswers = svmData.iloc[:, 0].values
svmFeatures = np.delete(svmData.as_matrix(), 0, 1)

svc = SVC(C=100000, kernel='linear', random_state=241)

svc.fit(svmFeatures, svmAnswers)

print svc.support_