import numpy as np
import math
from pandas import read_csv
from sklearn.metrics import roc_auc_score

C = 10

# градиентый шаг
def step(k, w, y, X, r):
    l = len(y)
    v1 = 0
    v2 = 0
    for i in xrange(0, l):
        yi = y[i]
        xi = X[i]
        v1 += yi * xi[0] * (1 - 1 / (1 + math.exp(-yi * (w[0] * xi[0] + w[1] * xi[1]))))
        v2 += yi * xi[1] * (1 - 1 / (1 + math.exp(-yi * (w[0] * xi[0] + w[1] * xi[1]))))

    print v1, v2

    d1 = 0
    d2 = 0
    if r:
        d1 = k * C * w[0]
        d2 = k * C * w[1]

    w[0] += k / l * v1 - d1
    w[1] += k / l * v2 - d2

    print w
    return w


def proba(w, x):
    return 1 / (1 + math.exp(-w[0] * x[0] - w[1] * x[1]))


def score(w_start, k, n, y, X, r):
    w = [ w_start[0], w_start[1] ]
    # обучение, расчёт весов
    for i in xrange(0, n):
        w = step(k, w, y, X, r)

    # предсказание вероятности класса на обученных весах
    l = len(y)
    y_probs = []
    for i in xrange(0, l):
        y_probs.append(proba(w, X[i]))

    return (w, roc_auc_score(y, y_probs))

data = read_csv('data-logistic.csv', header=None)
y = data.iloc[:,0].values
X = np.delete(data.as_matrix(), 0, 1)

n = 1000
# шаг
k = 0.1
# веса
w = [ 0, 0 ]

# без регуляризации
d_score = score(w, k, n, y, X, False)
# с регуляризацией
reg_score = score(w, k, n, y, X, True)

print d_score[0]
print d_score[1]

print reg_score[0]
print reg_score[1]