import numpy as np
from pandas import read_csv
from sklearn.cross_validation import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import log_loss
import matplotlib.pyplot as plt
from math import exp

def plot(test_loss, train_loss):
    plt.figure()
    plt.plot(test_loss[0], test_loss[1], 'r', linewidth=2)
    plt.plot(train_loss[0], train_loss[1], 'g', linewidth=2)
    plt.legend(['test', 'train'])
    plt.show()


def score_transform(score):
    return  1 / (1 + exp(-score))

def score_transform_arr(scores):
    return [ score_transform(x) for x in scores ]

data = read_csv('gdm_data.csv')
y = data['Activity'].values
X = np.delete(data.as_matrix(), 0, 1)


X_train, X_test, y_train, y_test = \
     train_test_split(X, y, test_size=0.8, random_state=241)

print 'train len: ', len(X_train), 'test len: ', len(X_test)

minTestLoss = 100
minTestLossIter = -1

for learning_rate in [ 1, 0.5, 0.3, 0.2, 0.1 ]:
    clf = GradientBoostingClassifier(learning_rate=learning_rate, n_estimators=250, verbose=True, random_state=241)
    clf.fit(X_train, y_train)

    if learning_rate == 0.2:
        train_score = clf.staged_decision_function(X_train)
        test_score = clf.staged_decision_function(X_test)

        i = 1
        train_loss = [ [], [] ]
        for score in train_score:
            y_predict = score_transform_arr(score)
            loss = log_loss(y_train, y_predict)
            train_loss[0].append(i)
            train_loss[1].append(loss)
            i += 1

        i = 1
        test_loss = [ [], [] ]
        for score in test_score:
            y_predict = score_transform_arr(score)
            loss = log_loss(y_test, y_predict)
            test_loss[0].append(i)
            test_loss[1].append(loss)
            i += 1
            if minTestLoss > loss:
                minTestLoss = loss
                minTestLossIter = i - 1

        plot(test_loss, train_loss)

print minTestLoss, minTestLossIter

clf = RandomForestClassifier(n_estimators=minTestLossIter, random_state=241)
clf.fit(X_train, y_train)

y_predict = clf.predict_proba(X_test)
loss = log_loss(y_test, y_predict)

print 'RFC loss: ', loss
