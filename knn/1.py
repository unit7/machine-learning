from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import KFold
from sklearn.cross_validation import cross_val_score
from sklearn.preprocessing import scale

import numpy as np
import pandas as ps

n_folds = 5

wines_table = ps.read_table('wine.data', sep=',')
classes = wines_table.iloc[:,0].values
properties = np.delete(wines_table.as_matrix(), 0, 1)

n = len(classes)

kf = KFold(n, n_folds, shuffle=True, random_state=42)

max_acc = 0
sel_k = 0

for k in xrange(1, 51):
    classifier = KNeighborsClassifier(n_neighbors=k)
    scores = cross_val_score(classifier, properties, classes, scoring='accuracy', cv=kf)
    scores_sum = sum(scores)
    loc_max_acc = scores_sum / n_folds
    if (loc_max_acc > max_acc):
        max_acc = loc_max_acc
        sel_k = k

print sel_k, max_acc

