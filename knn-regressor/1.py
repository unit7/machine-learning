from sklearn.datasets import load_boston
from sklearn.preprocessing import scale
from sklearn.neighbors import KNeighborsRegressor
from sklearn.cross_validation import KFold
from sklearn.cross_validation import cross_val_score

import numpy as np
import sys

data = load_boston()
boston_data = scale(data.data)
boston_target = data.target

n_folds = 5
n = len(boston_target)

kfolds = KFold(n, n_folds=n_folds, shuffle=True, random_state=42)

min_acc = -9999
p_ans = 0

for p in np.linspace(1, 10, 200):
    regressor = KNeighborsRegressor(n_neighbors=5, weights='distance', p=p, metric='minkowski')
    scores = cross_val_score(regressor, boston_data, boston_target, scoring='neg_mean_squared_error', cv=kfolds)
    scores_sum = sum(scores)
    loc_min_acc =  scores_sum / n_folds
    print scores, loc_min_acc, p
    if (loc_min_acc > min_acc):
        min_acc = loc_min_acc
        p_ans = p

print min_acc, p_ans