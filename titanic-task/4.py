import pandas;
import math;

passengers = pandas.read_csv('passengers.csv', index_col='PassengerId');
ageCol = passengers['Age'];

ageList = [];
ageSum = 0;

for age in ageCol:
    if not math.isnan(age):
        ageList.append(age);
        ageSum += age;

ageList.sort();

avgCount = len(ageList)
avgIndex = avgCount / 2

print ageSum / avgCount, ageList[avgIndex]