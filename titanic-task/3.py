import pandas;
import numpy;

passengers = pandas.read_csv('passengers.csv', index_col='PassengerId');
classCol = passengers['Pclass'];

result = classCol.value_counts();

allPassengersCount = numpy.sum(result.values)
firstClassCount = int(result[1]);

firstClassPercents = 100.0 * firstClassCount / allPassengersCount;

print firstClassPercents