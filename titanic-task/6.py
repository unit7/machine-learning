import pandas;

OTHER = 1;
WOMAN = 2;
MRS = 3;

def type(name):
    typeIndex = name.index(',') + 2;
    spaceIndex = name.index(' ', typeIndex);
    type = name[typeIndex:spaceIndex].lower();

    if 'mrs.' == type:
        return MRS;
    elif 'miss.' == type:
        return WOMAN;
    else:
        return OTHER;

def getName(name, type):
    if type == 2:
        return getMissName(name);
    elif type == 3:
        return getMrsName(name);
    else:
        return '';

def getMissName(name):
    nameStartIndex = name.index('Miss.') + 6;
    res = name[nameStartIndex:];
    return getOnlyName(res);

def getOnlyName(names):
    res = names
    nextSpaceIndex = res.find(' ');
    if (nextSpaceIndex != -1):
        res = res[:nextSpaceIndex];

    return res;

def getMrsName(name):
    nameStartIndex = name.find('(') + 1;
    res = name[nameStartIndex:len(name) - 1];
    return getOnlyName(res);

passengers = pandas.read_csv('passengers.csv', index_col='PassengerId');
nameCol = passengers['Name'];

names = {};

for name in nameCol:
    tp = type(name);
    womanName = getName(name, tp)
    if (len(womanName) != 0):
        count = names.get(womanName, 0) + 1
        names[womanName] = count

namesList = names.items();
namesList.sort(cmp=lambda f, s: s[1] - f[1])

print namesList