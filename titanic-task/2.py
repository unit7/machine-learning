import pandas;

passengers = pandas.read_csv('passengers.csv', index_col='PassengerId');
survivedCol = passengers['Survived'];

result = survivedCol.value_counts();

survived = int(result[1]);
dead = int(result[0]);
total = survived + dead;

survivedPercents = 100.0 * survived / total;

print survivedPercents