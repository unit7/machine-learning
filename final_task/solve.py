import datetime
import numpy as np
from pandas import read_csv
from scipy.sparse import hstack
from scipy.sparse import coo_matrix
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import KFold
from sklearn.cross_validation import cross_val_score

columns_to_delete = ['duration',
                     'tower_status_radiant',
                     'tower_status_dire',
                     'barracks_status_radiant',
                     'barracks_status_dire']

hero_columns = ['r1_hero', 'r2_hero',
                'r3_hero', 'r4_hero',
                'r5_hero', 'd1_hero',
                'd2_hero', 'd3_hero',
                'd4_hero', 'd5_hero']

categorical_features = ['lobby_type', 'r1_hero',
                        'r2_hero', 'r3_hero', 'r4_hero',
                        'r5_hero', 'd1_hero', 'd2_hero',
                        'd3_hero', 'd4_hero', 'd5_hero']


def rate_score(classifier, train_X, train_y):
    n_folds = 5
    n = len(train_y)
    cv = KFold(n, n_folds=n_folds, shuffle=True)

    start_time = datetime.datetime.now()

    val_score_list = cross_val_score(classifier, train_X, train_y, scoring='roc_auc', cv=cv)

    val_score = sum(val_score_list) / n_folds
    elapsed = datetime.datetime.now() - start_time

    return (val_score, elapsed)


def gradient_boosting_task():

    def find_skips(data):
        train_data_rows = len(data)
        non_nan_count = data.count()
        columns = non_nan_count.keys()
        with_skip_columns = []

        for c in columns:
            if non_nan_count[c] != train_data_rows:
                with_skip_columns.append(c)

        return with_skip_columns

    train_data = read_csv('features.csv', index_col='match_id')
    train_data.drop(columns_to_delete, axis=1, inplace=True)

    print find_skips(train_data)

    train_data.fillna('0', inplace=True)

    train_y = train_data['radiant_win']
    train_data.drop('radiant_win', axis=1, inplace=True)
    train_X = train_data

    n_estimators_list = [ 40]

    for n_estimators in n_estimators_list:
        clf = GradientBoostingClassifier(n_estimators=n_estimators, max_depth=3)
        (val_score, elapsed) = rate_score(clf, train_X, train_y)
        print n_estimators, val_score, elapsed


def logistic_regression_task():

    def exec_train(train_X, train_y):
        reg_c_list = [0.1, 0.2, 0.5, 1.0, 5.0, 10.0]

        for c in reg_c_list:
            clf = LogisticRegression(C=c)
            (val_score, elapsed) = rate_score(clf, train_X, train_y)
            print c, val_score, elapsed

    def find_hero_ids(train_data):
        hero_id_rows = train_data[hero_columns]
        return np.unique(hero_id_rows)

    def make_heroes_features(data, count):
        X_pick = np.zeros((data.shape[0], count))

        for i, match_id in enumerate(data.index):
            for p in xrange(5):
                X_pick[i, data.ix[match_id, 'r%d_hero' % (p + 1)] - 1] = 1
                X_pick[i, data.ix[match_id, 'd%d_hero' % (p + 1)] - 1] = -1

        return X_pick

    train_data = read_csv('features.csv', index_col='match_id')
    train_data.drop(columns_to_delete, axis=1, inplace=True)

    train_data.fillna('0', inplace=True)

    train_y = train_data['radiant_win']
    train_data.drop('radiant_win', axis=1, inplace=True)

    sc = StandardScaler()
    train_X = sc.fit_transform(train_data)

    exec_train(train_X, train_y)

    # remove categorical features
    train_data_without_cats = train_data.drop(categorical_features, axis=1, inplace=False)
    train_X = sc.fit_transform(train_data_without_cats)

    # try learn without categorical features
    exec_train(train_X, train_y)

    # count how many heroes was in the game, across all matches
    hero_ids = find_hero_ids(train_data)
    heroes_count = len(hero_ids)
    print 'heroes: ', heroes_count, hero_ids

    # create new heroes features
    heroes_features = make_heroes_features(train_data, max(hero_ids))

    # union all features with new heroes features
    train_X = coo_matrix(train_X)
    heroes_features = coo_matrix(heroes_features)

    train_X = hstack([train_X, heroes_features])
    exec_train(train_X, train_y)

    # predict result on test data with best model
    test_data = read_csv('features_test.csv', index_col='match_id')
    test_data.fillna('0', inplace=True)

    test_hero_ids = find_hero_ids(train_data)
    test_heroes_count = len(test_hero_ids)
    print 'test heroes: ', test_heroes_count, test_hero_ids

    test_heroes_features = make_heroes_features(test_data, max(test_hero_ids))
    test_data_without_cats = test_data.drop(categorical_features, axis=1, inplace=False)
    test_X = sc.fit_transform(test_data_without_cats)

    test_X = coo_matrix(test_X)
    test_heroes_features = coo_matrix(test_heroes_features)

    test_X = hstack([test_X, test_heroes_features])
    clf = LogisticRegression(C=1.0)
    clf.fit(train_X, train_y)
    predicted = clf.predict_proba(test_X)

    minPredict = 1
    maxPredict = 0

    for p in predicted:
        predict = p[1]
        if predict > maxPredict:
            maxPredict = predict
        if predict < minPredict:
            minPredict = predict

    print 'min predicted to radiant win: ', minPredict, 'max predicted: ', maxPredict

gradient_boosting_task()
# logistic_regression_task()