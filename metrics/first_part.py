import numpy as np
from pandas import read_csv
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score


data = read_csv('classification.csv')
answers = data.iloc[:, 0].values
predicted = np.delete(data.as_matrix(), 0, 1).T[0]

TP, FP, FN, TN = 0, 0, 0, 0

for i in xrange(0, len(answers)):
    true_answer = answers[i]
    predicted_answer = predicted[i]
    if true_answer:
        if predicted_answer:
            TP += 1
        else:
            FN += 1
    else:
        if predicted_answer:
            FP += 1
        else:
            TN += 1

print TP, FP, FN, TN

print accuracy_score(answers, predicted)
print precision_score(answers, predicted)
print recall_score(answers, predicted)
print f1_score(answers, predicted)