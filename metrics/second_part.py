import numpy as np
from pandas import read_csv
from sklearn.metrics import roc_auc_score
from sklearn.metrics import precision_recall_curve


data = read_csv('scores.csv')
answers = data.iloc[:, 0].values
scores = np.delete(data.as_matrix(), 0, 1).T

maxScore = 0
maxScoreIndex = -1

for i in xrange(0, len(scores)):
    nextScore = roc_auc_score(answers, scores[i])
    if nextScore > maxScore:
        maxScore = nextScore
        maxScoreIndex = i

print 'max_score', maxScore, ', index', maxScoreIndex, ', column', data.keys()[maxScoreIndex + 1]

maxPrecision = 0
maxPrecisionIndex = -1

for i in xrange(0, len(scores)):
    precision, recall, thresholds = precision_recall_curve(answers, scores[i])
    for j in xrange(0, len(precision)):
        p = precision[j]
        r = recall[j]
        if r >= 0.7:
            if p > maxPrecision:
                maxPrecision = p
                maxPrecisionIndex = i

print 'maxPrecision', maxPrecision, ', maxPrecisionIndex', maxPrecisionIndex, ', column', data.keys()[maxPrecisionIndex + 1]